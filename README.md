# Subject

The app allows you to choose a destination and a payment method (depending on the destination) to start a trip to the selected planet.

# Requirements

Write the Destination Selection part of the application, making sure that a user can start a trip (i.e, see an animation when clicking on the order submission button), following these requirements:

- use [redux](https://redux.js.org/), [redux-saga](https://github.com/redux-saga/redux-saga) and [styled-components](https://github.com/styled-components/styled-components)
- the code must be tested
- the code must match the visuals provided (no need for pixel-perfection) - see provided png files.
- Once a destination is selected, an action of type `order/SET_DESTINATION` must be dispatched.

Some `// TODO` comments are also present in the existing code, where it should rely on the destination selection implementation.

# Commands

```
  npm install

  npm start
    Starts the development server.

  npm test
    Starts the test runner.
```

# API

The `services` module simulates an API with a bit of latency.
In particular, the `destination` service exposes a `find` method returning destinations whose name starts with the string passed as argument, regardless of case.
