import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { List, Destination, Image, Button } from "./DestinationsList.styles";
import { getDestinations } from "../store/selectors";
import { setDestination } from "../store/actions";

const mapStateToProps = state => ({
  destinations: getDestinations(state)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      setDestination
    },
    dispatch
  )
});

const DestinationsList = ({
  onSelect,
  destinations: { list, found },
  actions: { setDestination }
}) => {
  const filtered = list.filter(d => found.includes(d.id));
  const handleOnSelect = id => {
    const destination = list.find(({ id: givenId }) => givenId === id);
    onSelect(id);
    setDestination(destination);
  };
  return (
    <List>
      {filtered.map(({ id, thumb, name }) => (
        <Destination key={id}>
          <Button onClick={() => handleOnSelect(id)}>
            <Image src={thumb} />
            {name}
          </Button>
        </Destination>
      ))}
    </List>
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DestinationsList);
