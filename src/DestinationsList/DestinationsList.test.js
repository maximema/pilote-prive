import React from "react";
import "jest-styled-components";
import renderer from "react-test-renderer";
import DestinationsList from "./DestinationsList";

describe("DestinationList/", () => {
  describe("#render", () => {
    it("renders correctly", () => {
      const tree = renderer.create(<DestinationsList />);
      expect(tree.toJSON()).toMatchSnapshot();
    });
  });
});
