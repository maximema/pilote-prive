import styled from "styled-components";

export const List = styled.ul`
  padding: 1rem;
`;

export const Destination = styled.li`
  list-style-type: none;
  padding: 0.3rem;
`;

export const Button = styled.button`
  background: none;
  border: none;
  color: #fff;
  font-family: "Passion One", sans-serif;
  font-size: 1.2rem;
  :focus {
    outline-width: 0;
  }
`;

export const Image = styled.img`
  width: 1.5rem;
  margin-right: 0.5rem;
`;
