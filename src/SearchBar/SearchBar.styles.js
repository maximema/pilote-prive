import styled from "styled-components";

export const Input = styled.input`
  font-family: "Passion One", sans-serif;
  font-size: 1.2rem;
  color: #fff;
  background: transparent;
  border: none;
  border-bottom: 1px solid yellow;
  :focus {
    outline-width: 0;
  }
`;
