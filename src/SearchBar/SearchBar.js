import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import { Input } from "./SearchBar.styles";
import { findDestinations } from "../store/actions";

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      findDestinations
    },
    dispatch
  )
});

const SearchBar = ({ onUpdate, actions }) => (
  <Input
    onChange={e => {
      actions.findDestinations(e.target.value);
      onUpdate();
    }}
  />
);

export default connect(
  null,
  mapDispatchToProps
)(SearchBar);
