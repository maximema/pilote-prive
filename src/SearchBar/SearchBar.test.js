import React from "react";
import "jest-styled-components";
import renderer from "react-test-renderer";
import SearchBar from "./SearchBar";

describe("search/", () => {
  describe("#render", () => {
    it("renders correctly", () => {
      const tree = renderer.create(<SearchBar />);
      expect(tree.toJSON()).toMatchSnapshot();
    });
  });
});
