import styled from "styled-components";

export const ContentWrapper = styled.section`
  width: 100%;
  display: flex;
  flex-direction: column;
`;
