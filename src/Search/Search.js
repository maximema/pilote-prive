import React from "react";

import { ContentWrapper } from "./Search.styles";
import SearchBar from "../SearchBar/SearchBar";
import DestinationsList from "../DestinationsList/DestinationsList";
import DestinationDetails from "../DestinationDetails/DestinationDetails";

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = { selectedDestinationId: null };
  }

  displayDetails = selectedDestinationId => {
    this.setState({
      selectedDestinationId
    });
  };

  displayList = () => {
    this.setState({
      selectedDestinationId: null
    });
  };

  render() {
    const { selectedDestinationId } = this.state;
    return (
      <ContentWrapper>
        <SearchBar onUpdate={this.displayList} />
        {selectedDestinationId ? (
          <DestinationDetails id={selectedDestinationId} />
        ) : (
          <DestinationsList onSelect={this.displayDetails} />
        )}
      </ContentWrapper>
    );
  }
}

export default Search;
