import React from "react";
import { shallow } from "enzyme";
import "jest-styled-components";
import renderer from "react-test-renderer";

import Search from "./Search";
import SearchBar from "../SearchBar/SearchBar";
import DestinationsList from "../DestinationsList/DestinationsList";

describe("search/", () => {
  const wrapper = shallow(<Search />);
  describe("#render", () => {
    it("renders <SearchBar />", () => {
      const searchBar = wrapper.find(SearchBar);
      expect(searchBar).toHaveLength(1);
    });

    it("renders <DestinationList />", () => {
      const searchBar = wrapper.find(DestinationsList);
      expect(searchBar).toHaveLength(1);
    });
  });
});
