import React from "react";
import { connect } from "react-redux";

import {
  Wrapper,
  Image,
  Details,
  Name,
  List,
  Item
} from "./DestinationDetails.styles";
import { getDestinations } from "../store/selectors";

const mapStateToProps = state => ({
  destinations: getDestinations(state)
});

const DestinationDetails = ({ id, destinations: { list } }) => {
  const destination = list.find(({ id: givenId }) => givenId === id);
  const { img, name, climate, gravity, population, price } = destination;
  return destination ? (
    <Wrapper>
      <Image src={img} />
      <Details>
        <Name>{name}</Name>
        <List>
          <Item>Climate: {climate}</Item>
          <Item>Gravity: {gravity}</Item>
          <Item>Population: {population}</Item>
          <Item>Price: {price}</Item>
        </List>
      </Details>
    </Wrapper>
  ) : null;
};

export default connect(mapStateToProps)(DestinationDetails);
