import styled from "styled-components";

export const Wrapper = styled.div`
  padding: 3rem;
  display: flex;
`;

export const Image = styled.img`
  width: 10rem;
  margin-right: 1rem;
  align-self: flex-start;
`;

export const Details = styled.div``;

export const Name = styled.h1``;
export const List = styled.ul`
  list-style-type: none;
`;
export const Item = styled.li``;
